/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.OrderDetail;
import com.werapan.databaseproject.model.Receipt;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptDao implements Dao<Receipt> {

    @Override
    public Receipt get(int id) {
        OrderDetailDao orderDetailDao = new OrderDetailDao();
        Receipt item = null;
        String sql = "SELECT * FROM receipt WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item  = Receipt.fromRS(rs);
               List<OrderDetail> orderDetails = orderDetailDao.getByOrderId(item.getId());
               item.setOrderDetails((ArrayList<OrderDetail>) orderDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item ;
    }

    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt item = Receipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Receipt> getAll(String where, String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt item = Receipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Receipt> getAll(String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt item = Receipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt save(Receipt obj) {
        OrderDetailDao orderDetailDao = new OrderDetailDao();
        String sql = "INSERT INTO receipt (receipt_total,receipt_qty,receipt_date)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        DatabaseHelper.beginTransaction();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotal());
            stmt.setInt(2, obj.getQty());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(3, sdf.format(obj.getDate()));
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            //Add detail
            obj.setId(id);
            for(OrderDetail od:obj.getOrderDetails()){
                OrderDetail detail = orderDetailDao.save(od);
                if(detail == null){
                    DatabaseHelper.endTransactionWithRollback();
                    return null;
                }
            }
            DatabaseHelper.endTransactionWithCommit();
            return get(id);
        } catch (SQLException ex) {
            DatabaseHelper.endTransactionWithRollback();
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Receipt update(Receipt obj) {
//        String sql = "UPDATE user"
//                + " SET user_login = ?, user_name = ?, user_gender = ?, user_password = ?, user_role = ?"
//                + " WHERE user_id = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getLogin());
//            stmt.setString(2, obj.getName());
//            stmt.setString(3, obj.getGender());
//            stmt.setString(4, obj.getPassword());
//            stmt.setInt(5, obj.getRole());
//            stmt.setInt(6, obj.getId());
////            System.out.println(stmt);
//            int ret = stmt.executeUpdate();
//            System.out.println(ret);
//            return obj;
//        } catch (SQLException ex) { 
//            System.out.println(ex.getMessage());
//            return null;
//        }
        return null;
    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM receipt WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
