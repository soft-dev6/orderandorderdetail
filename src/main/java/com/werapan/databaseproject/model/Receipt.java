/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hanam
 */
public class Receipt {
    private int id;
    private Date date;
    private double total;
    private int qty;
    private ArrayList<OrderDetail> orderDetails;

    public Receipt(int id, Date date, double total, int qty, ArrayList<OrderDetail> orderDetails) {
        this.id = id;
        this.date = date;
        this.total = total;
        this.qty = qty;
        this.orderDetails = orderDetails;
    }
    
    public Receipt(){
        this.id = -1;
        orderDetails = new ArrayList<>();
        qty=0;
        total=0;
        date = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public ArrayList<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(ArrayList<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }
    
    
    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", date=" + date + ", total=" + total + ", qty=" + qty +'}';
    }
    
    

    public void addOrderDeTail(OrderDetail orderDetail){
        orderDetails.add(orderDetail);
        total = total + orderDetail.getTotal();
        qty = qty + orderDetail.getQty();
    }
    
     public void addOrderDeTail(Product product1, String productName, double productPrice, int qty){
        OrderDetail orderDetail = new OrderDetail(product1,productName,productPrice,1,this);
        this.addOrderDeTail(orderDetail);
     }
     
     public void addOrderDeTail(Product product,int qty){
        OrderDetail orderDetail = new OrderDetail(product,product.getName(),product.getPrice(),1,this);
        this.addOrderDeTail(orderDetail);
     }
//     
     public static Receipt fromRS(ResultSet rs) {
        Receipt order = new Receipt();
        try {
            order.setId(rs.getInt("receipt_id"));
            order.setQty(rs.getInt("receipt_qty")); 
            order.setTotal(rs.getDouble("receipt_total"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            order.setDate(sdf.parse(rs.getString("receipt_date")));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }
}