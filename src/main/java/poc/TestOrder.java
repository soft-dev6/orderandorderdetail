/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poc;

import com.werapan.databaseproject.dao.ReceiptDao;
import com.werapan.databaseproject.model.OrderDetail;
import com.werapan.databaseproject.model.Product;
import com.werapan.databaseproject.model.Receipt;
import java.util.ArrayList;

/**
 *
 * @author hanam
 */
public class TestOrder {
    public static void main(String[] args) {
        Product product1 = new Product(1,"Espresso",30);
        Product product2 = new Product(2,"Americano",40);
        Product product3 = new Product(3,"C",50);
        Receipt order = new Receipt();
//        OrderDetail orderDetail1 = new OrderDetail(product1,product1.getName(),product1.getPrice(),1,order);
        order.addOrderDeTail(product1,1);
        order.addOrderDeTail(product2,1);
        order.addOrderDeTail(product3,1);
        
        System.out.println(order);
        System.out.println(order.getOrderDetails());
        
        ReceiptDao receiptDao = new ReceiptDao();
        Receipt newOrder = receiptDao.save(order);
        System.out.println(newOrder);
        
        Receipt order1 = receiptDao.get(newOrder.getId());
        printReceipt(order1);
       
    }
    
    static void printReceipt(Receipt order){
        System.out.println("Order" + order.getId());
        for(OrderDetail od:order.getOrderDetails()){
            System.out.println(" "+ od.getProductName()+ " " + od.getQty()
                   +" "+od.getProductPrice()+ " " + od.getTotal());
        }
        System.out.println("Total " + order.getTotal()+ "Qty: "+ order.getQty());
    }
}
